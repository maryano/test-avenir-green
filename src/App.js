import logo from './assets/images/logo-Avenir_Green.svg';
import './App.css';
import './assets/css/styles.css';
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link
} from "react-router-dom";
import Login from "./views/Login";

function App() {
  return (
    <div>
      <nav>
        <img class="avenir-logo" width="157px" height="83px" src={logo} />
      </nav>
      <Router>
        <switch>
          <Route exact path="/login">
            <Login />
          </Route>
          <Route exact path="/">
            <Login />
          </Route>
        </switch>
      </Router>
      <footer>
        By Avenir Green - Tout droits réservés
    </footer>
    </div>
  );
}

export default App;
