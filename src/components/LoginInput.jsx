import React, {useState} from 'react';
import shape from '../assets/images/Visible.png';

const LoginInput = () => {
    const [email, setEmail] = useState("");
    const handleEmail = (e) => {
        setEmail(e.currentTarget.value);
    }
   const [passwordVisible, setPasswordVisible] = useState(false);
   const handleVisibility = () => {
    setPasswordVisible(!passwordVisible);
   }
    return (
        <div className="login-forms">
            <input value={email} onChange={handleEmail} placeholder="EMAIL" name="email" type="email" />
            <div className="password-state">
                <input className="password-input no-margin-top" placeholder="MOT DE PASSE" name="password" type={!passwordVisible ? "password" : "text"} />
                <img onClick={handleVisibility} width="22px" src={shape}/>
            </div>
            <div className="text-center">
                <p className="text-semi-bold">Oups, mot de passe oublié ?</p>
                <button className="btn btn-green btn-connect" type="submit"> connexion </button>
            </div>
        </div>   
    );
}
export default LoginInput;