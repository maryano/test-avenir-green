import React, {useState} from 'react';
import illustration from '../assets/images/Illustration_Landing.png';
import LoginInput from '../components/LoginInput';

const Login = () => {
    const [email, setEmail] = useState("");
    const handleEmail = (e) => {
        setEmail(e.currentTarget.value);
    }
    return (
        <section className="main-content-login">
        <div className="content-login">
            <div className="illustration-container grid-element-1"> 
                <img width="100%" src={illustration}/> 
            </div>
            <div className="form-container grid-element-2"> 
                <h1 className="Bienvenue-dans-votre-espace-client">Bienvenue dans votre espace client</h1>
                <p className="propose-par-avenir-green ">Proposé par <span className="text-green">Avenir green</span></p>
                <LoginInput/>
                <p className="text-center">Vous rencontrez un problème? <a className="text-green text-underlined">Contactez nous</a></p>
            </div>
        </div>
    </section>
    );
}
export default Login;